package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.utils.FlowersToXml;
import com.epam.rd.java.basic.task8.utils.Validating;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);

        // PLACE YOUR CODE HERE
        ArrayList<Flower> dc_arr = domController.parseObjFromXml();

        // sort (case 1)
        // PLACE YOUR CODE HERE
        dc_arr.sort(Flower::compareTo);

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        FlowersToXml.parseObjToXml(outputXmlFile, dc_arr);
        System.out.println("Is " + outputXmlFile + " valid: " + Validating.validateXML(outputXmlFile, "input.xsd"));


        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        ArrayList<Flower> sax_arr = saxController.parseObjFromXml();

        // sort  (case 2)
        // PLACE YOUR CODE HERE
        sax_arr.sort(Flower::compareTo);

        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        FlowersToXml.parseObjToXml(outputXmlFile, sax_arr);
        System.out.println("Is " + outputXmlFile + " valid: " + Validating.validateXML(outputXmlFile, "input.xsd"));


        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        ArrayList<Flower> stax_arr = saxController.parseObjFromXml();

        // sort  (case 3)
        // PLACE YOUR CODE HERE
        stax_arr.sort(Flower::compareTo);

        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        FlowersToXml.parseObjToXml(outputXmlFile, stax_arr);
        System.out.println("Is " + outputXmlFile + " valid: " + Validating.validateXML(outputXmlFile, "input.xsd"));

        System.out.println("Is " + xmlFileName + " valid: " + Validating.validateXML(xmlFileName, "input.xsd"));

        xmlFileName = "invalidXML.xml";
        System.out.println("Is " + xmlFileName + " valid: " + Validating.validateXML(xmlFileName, "input.xsd"));

    }

}
