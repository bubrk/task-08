package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;


    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public ArrayList<Flower> parseObjFromXml() throws IOException, SAXException, ParserConfigurationException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();


        DocumentBuilder builder = factory.newDocumentBuilder();


        Document document = builder.parse(new File(xmlFileName));

        NodeList flowerElements = document.getDocumentElement().getElementsByTagName("flower");

        ArrayList<Flower> flowers = new ArrayList<>();

        if (flowerElements.getLength() > 0) {
            for (int i = 0; i < flowerElements.getLength(); i++) {
                Node flowerNode = flowerElements.item(i);
                flowers.add(getFlowerFromNode(flowerNode));
            }
        }

        return flowers;
    }

    private static Flower getFlowerFromNode(Node flowerNode) {

        Flower flower = new Flower();

        NodeList list = flowerNode.getChildNodes();

        if (list.getLength() > 0) {
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                switch (node.getNodeName()) {
                    case "name":
                        flower.setName(extractTextContent(node));
                        break;

                    case "soil":
                        flower.setSoil(Soil.getSoilByValue(extractTextContent(node)));
                        break;

                    case "origin":
                        flower.setOrigin(extractTextContent(node));
                        break;

                    case "visualParameters":
                        flower.setVisualParameters(extractVisualParameters(node));
                        break;

                    case "growingTips":
                        flower.setGrowingTips(extractGrowingTips(node));
                        break;

                    case "multiplying":
                        flower.setMultiplying(
                                Multiplying.getMultiplyingByValue(
                                        extractTextContent(node)));
                        break;

                    default:
                        break;
                }
            }
        }

        return flower;
    }

    private static VisualParameters extractVisualParameters(Node nodeVP) {
        NodeList list = nodeVP.getChildNodes();

        VisualParameters vp = new VisualParameters();

        if (list.getLength() > 0) {
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                switch (node.getNodeName()) {
                    case "stemColour":
                        vp.setStemColour(extractTextContent(node));
                        break;
                    case "leafColour":
                        vp.setLeafColour(extractTextContent(node));
                        break;
                    case "aveLenFlower":
                        Measurement m = new Measurement(Integer.parseInt(extractTextContent(node)),
                                node.getAttributes().getNamedItem("measure").getNodeValue());
                        vp.setAveLenFlower(m);
                        break;
                }
            }
        }
        return vp;
    }

    private static GrowingTips extractGrowingTips(Node nodeGT) {
        NodeList list = nodeGT.getChildNodes();

        GrowingTips gt = new GrowingTips();

        if (list.getLength() > 0) {
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                switch (node.getNodeName()) {
                    case "tempreture":
                        {Measurement m = new Measurement(Integer.parseInt(extractTextContent(node)),
                                                            node.getAttributes().getNamedItem("measure").getNodeValue());
                        gt.setTemperature(m);}
                        break;
                    case "lighting":
                        gt.setLightening(LightRequiring.valueOf(node.getAttributes()
                                                                    .getNamedItem("lightRequiring")
                                                                    .getNodeValue()));
                        break;
                    case "watering":
                        {Measurement m = new Measurement(Integer.parseInt(extractTextContent(node)),
                                                            node.getAttributes().getNamedItem("measure").getNodeValue());
                        gt.setWatering(m);};
                        break;
                }
            }
        }
        return gt;
    }

    private static String extractTextContent(Node node) {

        return node.getTextContent()
                .replace("\n", "")
                .trim();
    }

}
