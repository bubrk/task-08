package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.epam.rd.java.basic.task8.entity.Multiplying.getMultiplyingByValue;
import static com.epam.rd.java.basic.task8.entity.Soil.getSoilByValue;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private static ArrayList<Flower> flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public ArrayList<Flower> parseObjFromXml() throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();

		XMLHandler handler = new XMLHandler();
		parser.parse(new File(xmlFileName), handler);
		return flowers;
	}



	private static class XMLHandler extends DefaultHandler {
		//private LinkedList<String> correctSequence;
		private String currentInformation;
		private Attributes currentAttributes;
		Flower flower;

//		String[] SEQUENCE = {"flower",
//								"name","name",
//								"soil","soil",
//								"origin","origin",
//								"visualParameters",
//									"stemColour","stemColour",
//									"leafColour","leafColour",
//									"aveLenFlower","aveLenFlower",
//								"visualParameters",
//								"growingTips",
//									"tempreture","tempreture",
//									"lighting","lighting",
//									"watering","watering",
//								"growingTips",
//								"multiplying","multiplying",
//							 "flower","flowers"};

		@Override
		public void startDocument() throws SAXException {

		}

		@Override
		public void endDocument() throws SAXException {

		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//			if (!isReservedName(qName)){
//				throw new SAXException("Unknown tag name: "+qName);
//			}
			if ("flowers".equals(qName)){
				flowers = new ArrayList<>();
			} //else {

//				if (!checkCorrectSequence(qName)){
//					throw new SAXException("Sequence of the tags is not correct");
//				}
//			}

			switch (qName) {
//				case "flowers": initCorrectSequence();
//								break;
				case "flower": flower = new Flower();
								break;
				case "visualParameters": flower.setVisualParameters(new VisualParameters());
											break;
				case "growingTips": flower.setGrowingTips(new GrowingTips());
									break;
				default: break;
			}
			currentInformation="";

			currentAttributes = attributes;
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
//			if (!isReservedName(qName)){
//				throw new SAXException("Unknown tag name: "+qName);
//			}
//			if ("flowers".equals(qName)){
//
//			} else {
//
//				if (!checkCorrectSequence(qName)){
//					throw new SAXException("Sequence of the tags is not correct");
//				}
//			}

			switch (qName) {
				case "flower":  if (flower.isFullyFilled()){
									flowers.add(flower);
											} else {
												throw new SAXException("Some fields are not present or empty");
											}
								//initCorrectSequence();
								break;

				case "name": 	flower.setName(currentInformation);
								break;

				case "soil": 	flower.setSoil(getSoilByValue(currentInformation));
								break;

				case "origin":  flower.setOrigin(currentInformation);
								break;

				case "stemColour": {VisualParameters vp = flower.getVisualParameters();
									vp.setStemColour(currentInformation);
									flower.setVisualParameters(vp);}
								break;

				case "leafColour": {VisualParameters vp = flower.getVisualParameters();
									vp.setLeafColour(currentInformation);
									flower.setVisualParameters(vp);}
								break;

				case "aveLenFlower": {VisualParameters vp = flower.getVisualParameters();
											Measurement m = new Measurement();
											m.setValue(Integer.parseInt(currentInformation));
											m.setMeasure(currentAttributes.getValue("measure"));
										vp.setAveLenFlower(m);
										flower.setVisualParameters(vp);}
								break;


				case "tempreture": {GrowingTips gt = flower.getGrowingTips();
										Measurement m = new Measurement();
										m.setValue(Integer.parseInt(currentInformation));
										m.setMeasure(currentAttributes.getValue("measure"));
										gt.setTemperature(m);
									flower.setGrowingTips(gt);
									}
								break;

				case "lighting": {GrowingTips gt = flower.getGrowingTips();
									LightRequiring lr = LightRequiring.valueOf(currentAttributes.getValue("lightRequiring"));
									gt.setLightening(lr);
									flower.setGrowingTips(gt);
									}
								break;

				case "watering": {GrowingTips gt = flower.getGrowingTips();
									Measurement w = new Measurement();
									w.setValue(Integer.parseInt(currentInformation));
									w.setMeasure(currentAttributes.getValue("measure"));
									gt.setWatering(w);
									flower.setGrowingTips(gt);
									}
								break;

				case "multiplying": flower.setMultiplying(getMultiplyingByValue(currentInformation));
									break;

				default: break;
			}

		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String information = new String(ch, start, length);

			information = information.replace("\n", "").trim();

			if (!information.isEmpty()) {
				this.currentInformation = information;
			}
		}

		@Override
		public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		}

//		private boolean isReservedName(String qName) {
//			return Arrays.asList(SEQUENCE).contains(qName);
//		}
//
//		private void initCorrectSequence(){
//			correctSequence = new LinkedList<>(Arrays.asList(SEQUENCE));
//		}
//
//		private boolean checkCorrectSequence(String qName){
//			if (qName.equals(correctSequence.pop())){
//				return true;
//			} else {
//				if ("growingTips".equals(qName) || "visualParameters".equals(qName)){
//					correctSequence.pop(); //skip element
//					return qName.equals(correctSequence.pop());
//				}
//			}
//
//			return false;
//		}
	}


}