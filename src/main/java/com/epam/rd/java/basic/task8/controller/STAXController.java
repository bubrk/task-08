package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.epam.rd.java.basic.task8.entity.Multiplying.getMultiplyingByValue;
import static com.epam.rd.java.basic.task8.entity.Soil.getSoilByValue;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private static ArrayList<Flower> flowers;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Flower> parseObjFromXml() throws ParserConfigurationException, SAXException, IOException, XMLStreamException {

		ArrayList<Flower> flowers = new ArrayList<>();

		Flower flower = new Flower();

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();


		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

		while (reader.hasNext()) {

			XMLEvent xmlEvent = reader.nextEvent();

			if (xmlEvent.isStartElement()) {
				StartElement startElement = xmlEvent.asStartElement();

				String elementName = startElement.getName().getLocalPart();

				switch (elementName){
					case "flower":
						flower = new Flower();
						break;
					case "visualParameters":
						flower.setVisualParameters(new VisualParameters());
						break;
					case "growingTips":
						flower.setGrowingTips(new GrowingTips());
						break;

					case "name":
						xmlEvent = reader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
						break;

					case "soil":
						xmlEvent = reader.nextEvent();
						flower.setSoil(getSoilByValue(xmlEvent.asCharacters().getData()));
						break;

					case "origin":
						xmlEvent = reader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
						break;

					case "stemColour":
						{	xmlEvent = reader.nextEvent();
							VisualParameters vp = flower.getVisualParameters();
							vp.setStemColour(xmlEvent.asCharacters().getData());
							flower.setVisualParameters(vp);
						}
					break;

					case "leafColour":
						{	xmlEvent = reader.nextEvent();
							VisualParameters vp = flower.getVisualParameters();
							vp.setLeafColour(xmlEvent.asCharacters().getData());
							flower.setVisualParameters(vp);
						}
					break;

					case "aveLenFlower":
						{	xmlEvent = reader.nextEvent();
							VisualParameters vp = flower.getVisualParameters();
								Measurement m = new Measurement();
								m.setValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
								m.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
							vp.setAveLenFlower(m);
							flower.setVisualParameters(vp);
						}
					break;


					case "tempreture":
						{	xmlEvent = reader.nextEvent();GrowingTips gt = flower.getGrowingTips();
								Measurement m = new Measurement();
								m.setValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
								m.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
							gt.setTemperature(m);
							flower.setGrowingTips(gt);
						}
					break;

					case "lighting":
						{	xmlEvent = reader.nextEvent();GrowingTips gt = flower.getGrowingTips();
							LightRequiring lr = LightRequiring.valueOf(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
							gt.setLightening(lr);
							flower.setGrowingTips(gt);
						}
					break;

					case "watering":
						{	xmlEvent = reader.nextEvent();GrowingTips gt = flower.getGrowingTips();
								Measurement w = new Measurement();
								w.setValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
								w.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
								gt.setWatering(w);
							flower.setGrowingTips(gt);
						}
					break;

					case "multiplying":
						xmlEvent = reader.nextEvent();
						flower.setMultiplying(getMultiplyingByValue(xmlEvent.asCharacters().getData()));
						break;

				}

			}

			if (xmlEvent.isEndElement()) {

				EndElement endElement = xmlEvent.asEndElement();

				if (endElement.getName().getLocalPart().equals("flower")) {
					flowers.add(flower);
				}
			}
		}

		return flowers;
	}


}