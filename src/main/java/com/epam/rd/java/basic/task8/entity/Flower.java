package com.epam.rd.java.basic.task8.entity;

public class Flower implements Comparable{

    private String name;
    private Soil soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplying multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(Multiplying multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil=" + soil.getType() +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying=" + multiplying.getMethod() +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Flower another = (Flower) o;
        return this.getName().compareTo(another.getName());
    }

    public boolean isFullyFilled(){
        return (this.name!=null && !this.name.isEmpty() &&
                this.soil!=null &&
                this.origin!=null && !this.origin.isEmpty() &&
                this.visualParameters!=null && this.visualParameters.isFullyFilled() &&
                this.growingTips!=null && this.growingTips.isFullyFilled() &&
                this.multiplying!=null);
    }
}

