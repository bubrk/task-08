package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    Measurement temperature;
    LightRequiring lightening;
    Measurement watering;

    public GrowingTips() {

    }

    public GrowingTips(Measurement temperature, LightRequiring lightening, Measurement watering) {
        this.temperature = temperature;
        this.lightening = lightening;
        this.watering = watering;
    }

    public Measurement getTemperature() {
        return temperature;
    }

    public void setTemperature(Measurement temperature) {
        this.temperature = temperature;
    }

    public LightRequiring getLightening() {
        return lightening;
    }

    public void setLightening(LightRequiring lightening) {
        this.lightening = lightening;
    }

    public Measurement getWatering() {
        return watering;
    }

    public void setWatering(Measurement watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lightening=" + lightening +
                ", watering=" + watering +
                '}';
    }

    public boolean isFullyFilled() {
        return (this.temperature!=null && this.temperature.isFullyFilled() &&
                this.lightening!=null);
    }
}
