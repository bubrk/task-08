package com.epam.rd.java.basic.task8.entity;

public class Measurement {
    Integer value;
    String measure;

    public Measurement() {

    }

    public Measurement(int value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }

    public boolean isFullyFilled() {
        return (this.value !=null && this.value >0 &&
                this.measure!=null && !this.measure.isEmpty());
    }
}
