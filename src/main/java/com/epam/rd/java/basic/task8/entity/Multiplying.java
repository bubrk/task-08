package com.epam.rd.java.basic.task8.entity;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public enum Multiplying {
    LEAVES("листья"),
    CUTTINGS("черенки"),
    SEEDS("семена");

    private final String method;

    Multiplying(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public static Multiplying getMultiplyingByValue(String value) {
        for (Multiplying m: Multiplying.values()) {
            if (value.equals(m.getMethod())){
                return m;
            }
        }
        return null;
    }

}