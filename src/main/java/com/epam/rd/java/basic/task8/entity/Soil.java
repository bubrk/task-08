package com.epam.rd.java.basic.task8.entity;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public enum Soil {
    SUBGOLDEN("подзолистая"),
    UNPOVED("грунтовая"),
    TURF_SUBGOLDEN("дерново-подзолистая");

    private final String type;

    Soil(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static Soil getSoilByValue(String value) {
         for (Soil s: Soil.values()) {
            if (value.equals(s.getType())){
                return s;
            }
        }
         return null;
    }
}
