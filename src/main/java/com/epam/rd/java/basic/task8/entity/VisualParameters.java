package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    String stemColour;
    String leafColour;
    Measurement aveLenFlower;

    public VisualParameters(){

    }

    public VisualParameters(String stemColour, String leafColour, Measurement aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public Measurement getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(Measurement aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    public boolean isFullyFilled() {
        return (this.stemColour!=null && !this.stemColour.isEmpty() &&
                this.leafColour!=null && !this.leafColour.isEmpty());
    }
}
