package com.epam.rd.java.basic.task8.utils;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FlowersToXml {

    public static void parseObjToXml(String fileName, List<Flower> flowers) throws IOException {
        FileWriter fw = new FileWriter(fileName);

        fw.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<flowers xmlns=\"http://www.nure.ua\"\n" +
                "         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "         xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");

        for (Flower flower : flowers) {

            fw.append(getXmlFromFlower(flower));
            fw.append("\n");
        }

        fw.append("</flowers>");
        fw.close();
    }

    private static String getXmlFromFlower(Flower flower) {
        StringBuilder sb = new StringBuilder();

        sb.append("    <flower>\n")
          .append("        <name>")
                .append(flower.getName())
          .append("</name>\n")
          .append("        <soil>")
                .append(flower.getSoil().getType())
          .append("</soil>\n")
          .append("        <origin>")
                .append(flower.getOrigin())
          .append("</origin>\n")
          .append("        <visualParameters>\n")
          .append("            <stemColour>")
                .append(flower.getVisualParameters().getStemColour())
          .append("</stemColour>\n")
          .append("            <leafColour>")
                .append(flower.getVisualParameters().getLeafColour())
          .append("</leafColour>\n")
          .append("            <aveLenFlower measure = \"")
                .append(flower.getVisualParameters().getAveLenFlower().getMeasure())
                .append("\">")
                .append(flower.getVisualParameters().getAveLenFlower().getValue())
          .append("</aveLenFlower>\n")
          .append("        </visualParameters>\n")
          .append("        <growingTips>\n")
          .append("            <tempreture measure = \"")
                .append(flower.getGrowingTips().getTemperature().getMeasure())
                .append("\">")
                .append(flower.getGrowingTips().getTemperature().getValue())
                .append("</tempreture>\n")
          .append("            <lighting lightRequiring = \"")
                .append(flower.getGrowingTips().getLightening())
                .append("\"/>\n")
          .append("            <watering measure = \"")
                .append(flower.getGrowingTips().getWatering().getMeasure())
                .append("\">")
                .append(flower.getGrowingTips().getWatering().getValue())
          .append("</watering>\n")
          .append("        </growingTips>\n")
          .append("        <multiplying>")
                .append(flower.getMultiplying().getMethod())
          .append("</multiplying>\n")
          .append("    </flower>\n");

        return sb.toString();
    }
}
